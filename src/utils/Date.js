export let dateNow = ()=> new Date().toISOString().split("T")[0]

export let matchDate=(rawString)=>new Date(rawString).toISOString()